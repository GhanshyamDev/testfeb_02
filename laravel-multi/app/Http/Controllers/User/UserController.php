<?php

namespace App\Http\Controllers\User;

use Illuminate\Support\Facades\DB;
use App\Providers\RouteServiceProvider;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\State;
use App\City;
use App\Customer;
use App\UserProducts;
use App\Products;
use App\SendOTP;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Crypt;
class UserController extends Controller
{
    
 
   public function __construct(){
          // $noti = \DB::table('notifications')->get();
          //   $data['countNotify'] = $noti->count();
          //   return view('layouts.customer', $data);
   }

    public function index(){
    	
    	  if (!session()->has('Customer_logged')) {
           
              return view('users.login');
           } 
         return redirect('users/dashboard')->with("failed", 'Oppes! You have entered invalid credentials');
    }
  
    public function register(){
    	
    	  if (!session()->has('Customer_logged')) {
            
              $data['states'] = State::get();
              return view('users.register', $data);
           } 
         return redirect('users/dashboard')->with("failed", 'Oppes! You have entered invalid credentials');
    }
 


    public function authenticate(Request $request)
    {

        $request->validate([
            'email' => 'required|string|email',
            'password' => 'required|string',
        ]);
          $users = Customer::where('email', $request->email)->first();
		      if($users) {

		          if(Hash::check($request->password, $users['password'])) {
		                   $userSession= [
		                    'cust_id' =>$users->cust_id,
		                    'name' =>$users->name,
		                    'email' =>$users->email,
		                    'password' =>$users->password ,    
		                    'is_customer' =>$users->is_customer ];    

		                 session()->put('Customer_logged', $userSession);

		              return redirect('users/dashboard')->with('success', 'Login Successfully');

		            } else {

		              return back()->with("failed", "Invalid Password");
		            }
		         }
                    
          return back()->with("failed", 'Oppes! You have entered invalid credentials');
 

    }
  
    public function signUp(Request $request){
       $request->validate([
            'name' => 'required|string',
            'email' => 'required|unique:customer,email',
            'mobile' => 'required|max:10|unique:customer,mobile',
            'password' => 'required|alpha_num|min:6',
            'state' => 'required',
            'city' => 'required',
            'pincode' => 'required'
         ]);
          $dataArray       =       array(
            "name"         =>      $request->name,
            "email"        =>      $request->email,
            "mobile"       =>      $request->mobile, 
            "password"     =>      Hash::make($request->password), 
            "state"        =>      $request->state, 
            "city"         =>      $request->city, 
            "pincode"      =>      $request->pincode  
         );
            $user    =   Customer::create($dataArray);
        if(!is_null($user)) {
           //SendOTP::SendCode($request->mobile);
            $user->code =  SendOTP::SendCode($request->mobile);
            $users = Customer::where('cust_id', $user->id)->first();
            Customer::where('cust_id', $users->cust_id)->update(array('otp_verify'=>$user->code));
                     $userSession= [
                        'cust_id' =>$users->cust_id,
                        'name' =>$users->name,  ];    

                     session()->put('OTP_Verify', $userSession);

        return redirect('users/verify')->with([ 'success'=> 'Otp Send On Your Mobile No']);     
        }

        else {
            return back()->with("failed", "Alert! Failed to register");
        }
     
          return back()->with("failed", 'Oppes! You have entered invalid credentials');
  }



   public function mobileVerify()
    { 
      if (session()->has('OTP_Verify')) {
           
       return view('users.verify');
       } 
    return redirect('/users')->with("failed", 'Oppes! Required Field Is Empty');
    
    }

     public function postVerify(Request $request)
      { 
        $request->validate([
            'otp' => 'required' 
         ]);
        $data   =   array(
            "activate"         =>   1, 
         );
       // $city = Customer::where('id', $request->id)->update($data);
         $otp = Customer::where('cust_id', $request->cust_id)->where('otp_verify', $request->otp)->first();
         
        if($otp) {
            $verify = Customer::where('cust_id', $request->cust_id)->update($data);
             session()->flush(); 
            if($verify){
             
               $userSession= [
                'cust_id' =>$otp->cust_id,
                'name' =>$otp->name, 
                'email' =>$otp->email, 
                'mobile' =>$otp->mobile, 
                'city' =>$otp->city, 
                'state' =>$otp->state, 
                'pincode' =>$otp->pincode, 
                'is_customer' =>$otp->is_customer, 
                 ];    
              session()->put('Customer_logged', $userSession);
              return redirect('users/dashboard')->with([ 'success'=> 'Registration Successfully']);     
            }  
          } else{
            return back()->with([ 'failed'=> 'Otp Does Not Match']); 
          }
      }



    public function dashboard()
    { 

        if (session()->has('Customer_logged')) {
        
          
         
         $data['userProduct'] =UserProducts::where('userid', session('Customer_logged')['cust_id'])->leftJoin('tbl_products as p', 'p.product_id', '=', 'tbl_user_products.product_id')->leftJoin('tbl_category as cat','cat.category_id', '=','p.category_id' )->get()->toArray();
 
         return view('users.dashboad', $data);
          
        }else {

           return redirect('/users')->with("failed", 'Oppes! You have entered invalid credentials');
        }
    	
    }
  
   public function ViewUserProductDetails($id) {

      
        $product  = Products::where('product_id',Crypt::decryptString($id))->get()->toArray();
        $data['userProductDetails'] = UserProducts::join('tbl_products as p', 'p.product_id', '=', 'tbl_user_products.product_id')
	            ->join('tbl_category as cat', 'cat.category_id', '=', 'tbl_user_products.category_id')
              ->join('customer as cust', 'cust.cust_id', '=' ,'tbl_user_products.userid')
	            ->where('p.product_id', '=', $product[0]['product_id'])
	            ->where('tbl_user_products.product_id', '=', $product[0]['product_id'])
	             ->first()->toArray();

              $data['city'] = CIty::where('city_id', $data['userProductDetails']['city'])->first()->toArray();
             $data['state'] = State::where('state_id', $data['userProductDetails']['state'])->first()->toArray();   
	             
	      return view('users.user-product-details',$data);   
    }
    

    public function getCitys(Request $request){
         
           $city = City::where('state_id', $request->state)->get()->toArray() ;
            return response()->json(['success'=>'Got Simple Ajax Request.', 'citys'=>$city]);
      }

 
    public function logout()
      {
           session()->flush();   

        return redirect('users/')->with("error", 'Logout Successfully');
      }



}
