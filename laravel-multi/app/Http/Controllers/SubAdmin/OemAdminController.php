<?php

namespace App\Http\Controllers\SubAdmin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Http\Controllers\Auth;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Crypt;
use App\Products;
use App\Category;
use App\State;
use App\Role;
use App\Permission;
use App\OEMUser;
use App\User;
use Illuminate\Support\Facades\Hash;
class OemAdminController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {  
        // notify()->preset('user-updated');
      $user = User::find(1);
    $data['notifications'] = $user->unreadNotifications;
 
       //return $notifications = auth()->user()->unreadNotifications;  
    return view('subAdmin.dashboad', $data);
    }

    
    public function CreateUser(Request $request)
    {     

        if ($request->isMethod('post')) {

             $validatedData = $request->validate([
                  
                'name' => 'required',
                'email'=> 'required|unique:users',
                'phone'=> 'required|unique:users',
                'password' => 'required|min:6' ,
                'category' => 'required' ,
                'geography' => 'required' ,
            ]);
             if($validatedData){

                 $user = new User();
                 $user->name = $request->name;
                 $user->email =  $request->email;
                 $user->phone =  $request->phone;
                 $user->is_user =  1;
                 $user->password =Hash::make($request->password);
                 $user->category_for =  $request->category;
                 $user->geography =  $request->geography;
                $user->save();
                if($user->save())
                {
                   return redirect()->back()->with('success', 'User Create Successfully!');
                   // for ($i = 0; $i < count($request->permission); $i++) {
                            
                   //          DB::table('users_permissions')
                   //            ->updateOrInsert(
                   //                ['user_id' => $user->id ,'permission_id' => $request->permission[$i]] 
                   //            );
                               
                        
                   //      }
                   //  DB::table('users_roles')->insert(['user_id' => $user->id, 'role_id'=> $request->roleID]);
                   // DB::table('users_permissions')->insert(['user_id' => $user->id, 'permission_id'=> $request->permission]);
                }else {
                  return redirect()->back()->with('failed', 'Failed to Create User!');
                }
               
                 
              } 
          
            
         } else {
       
         $data['states'] = State::get();
         $data['roles']  =  Role::get()->toArray();
         $data['permissions']  =  Permission::get()->toArray();
         $data['oemUsers']  =  User::where('is_user', 1)->get()->toArray();
       
          return view('subAdmin.create-user',$data ); 
      }  
        
    }
    public function EditUser($id)
    { 
          $data['states'] = State::get();
         $data['roles']  =  Role::get()->toArray();
          $data['permissions']  =  Permission::get()->toArray();
         $data['oemUsers']  =  User::where('is_user', 1)->get()->toArray();

         $data['asigne'] =User::leftJoin('users_roles as rols', 'rols.user_id', '=', 'users.id')
         ->leftJoin('users_permissions as pr','pr.user_id', '=','users.id' )
         ->where('users.id', '=', Crypt::decryptString($id))
         ->get()->toArray();
         
         return view('subAdmin.edit-user',$data ); 
    } 



    public function updateUser(Request $request)
    { 
       
         $update= [
          'id' =>$request->user_id,
          'name' =>$request->name,
          'email' =>$request->email,
          'phone' =>$request->phone,
          'category_for' =>$request->category,
          'geography' =>$request->geography 
        ];
           User::where('id', $request->user_id)->update($update);
    
         // for ($i = 0; $i < count($request->permission); $i++) {
         //    $answers[] = [
         //     'user_id' => $request->user_id,
         //     'permission_id' => $request->permission[$i], 
         //    ];
         //    DB::table('users_permissions')
         //      ->updateOrInsert(
         //          ['user_id' => $request->user_id ,'permission_id' => $request->permission[$i]] 
         //      );
         // }
         
     return redirect('oemAdmin/userCreate')->with('success', 'User Update Successfully!');

     
    }


     public function DeleteUser($id)
    { 
      
     User::where('id', Crypt::decryptString($id))->delete();
     DB::table('users_permissions')->where('user_id', '=',Crypt::decryptString($id))->delete();
     DB::table('users_roles')->where('user_id', '=',Crypt::decryptString($id))->delete();
      return redirect('oemAdmin/userCreate')->with('success', 'User Delete Successfully!');
    }




    public function ApproveUser(Request $request)
    {
       $user  = User::where('id', $request->oemuid)->update(['approve' => $request->value]);
       if($user){
          return response()->json(array( 'success' => true,'data'   => 'success' ));    
       }else {
         return response()->json(array( 'failed' => false,'data'   => 'failed' ));      
       }
       
    }
   
  public function viewRenew_amcList(Request $request)
    {

     $data['renewlit'] =Products::join('renew_service as ren', 'ren.product_id', '=', 'tbl_products.product_id')
            ->join('tbl_user_products as usrp', 'usrp.product_id', '=', 'tbl_products.product_id')
              ->join('customer as cust', 'cust.cust_id', '=', 'ren.cust_id')
             ->join('tbl_cities as city','city.city_id', '=' ,'cust.city')
             ->get()->toArray();
   
            // echo '<pre>';
            //  print_r($data);
      return view('subAdmin.renew-amc-list', $data);
         
    }
   
    public function viewService_request(Request $request)
    {
      $data['renewlit'] =Products::join('renew_service as ren', 'ren.product_id', '=', 'tbl_products.product_id')
            ->join('tbl_user_products as usrp', 'usrp.product_id', '=', 'tbl_products.product_id')
              ->join('customer as cust', 'cust.cust_id', '=', 'ren.cust_id')
             ->join('tbl_cities as city','city.city_id', '=' ,'cust.city')
             ->get()->toArray();
           
            return view('subAdmin.view-service-request', $data);

    }
    

     public function createCategory(Request $request){
      
      if ($request->isMethod('post')) {

          $validatedData = $request->validate(['category' => 'required'  ]);
            
           $imageName = time().'.'.$request->logo->extension();  
            $request->logo->move(public_path('img/category/'), $imageName);

            $category = new Category();
            $category->category = $request->category;
            $category->logo     = $imageName;
            $category->save();
             
            return redirect()->back()->with('success', 'Category Create Successfully!'); 
      }else{
         
          $data['categories']=Category::get();
        return view('subAdmin.create-category',$data);

      } 
      

     }

     public function categoryEdit( $id){
 
       $data['getCategory'] = Category::where('category_id', Crypt::decryptString($id))->get()->toArray();
       $data['categoryEdit'] = $id;                  
       return view('subAdmin.create-category',$data);  
        
     }

   public function categoryUpdate(Request $request){

          
          if($request->logoUpdate > 0 ){
             
             $imageName = time().'.'.$request->logoUpdate->extension();  
             $request->logoUpdate->move(public_path('img/category/'), $imageName);
             $update= [
               'category' =>$request->category,
              'logo'     => $imageName ];
            
          }else
          {
             $update= [  'category' =>$request->category  ];
          
          }
          Category::where('category_id', $request->category_id)->update($update);
         return redirect('oemAdmin/createCategory')->with('success', 'Category Update Successfully!');
   }

   public function categoryDel($id){
      
      Category::where('category_id', Crypt::decryptString($id))->delete();
      return redirect('oemAdmin/createCategory')->with('success', 'Category Delete Successfully!');
   }


   public function createGeography(Request $request){
     
     if($request->isMethod('post')) {
            $validatedData = $request->validate(['geography' => 'required'  ]); 
            $data=[ 'name' => $request->geography, 'created_at' => now()   ];
         
            DB::table('geography')->insert($data);
            return redirect()->back()->with('success', 'Geography Create Successfully!'); 

     }else{
         $data['geography']=DB::table('geography')->get()->toArray();
        return view('subAdmin.create-geography',$data);         
     }
   }


   public function geographyEdit($id)
   {
       $data['geography'] = DB::table('geography')->where('id', Crypt::decryptString($id))->get()->toArray();
       $data['geographyEdit'] = $id;                  
       return view('subAdmin.create-geography',$data); 
   }

   public function geographyUpdate(Request $request){
       
           $update= ['name' =>$request->geography ];
           DB::table('geography')->where('id', $request->id)->update($update);
           return redirect('oemAdmin/createGeography')->with('success', 'Geography Update Successfully!');
   }

  public function geographyDel($id){
      
      DB::table('geography')->where('id', Crypt::decryptString($id))->delete();
      return redirect('oemAdmin/createGeography')->with('success', 'Geography Delete Successfully!');
   }

}
