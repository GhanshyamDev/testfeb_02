<?php

namespace App\Http\Controllers;

use App\Notifications\NotificationCreated;
use Illuminate\Http\Request;
use App\Notifications\NewPostNotify;
use Illuminate\Notifications\Notifiable;
use Illuminate\Support\Facades\Session;
use App\User;
class Notification extends Controller
{ 
       public function sendServiceNotification(Request $request)
  {    

  	 $users = User::first();
  	    $user = [
  	     'user_id' =>1,
  	     'name'   => 'SOnu Mishra',
  	     'email' =>  'ss@gmail.com',	
  	     ];
     $users->notify(new NotificationCreated($user));
      notify()->preset('user-updated');
  	     //  notify()->preset('user-updated');
       session::flash('success','Notification Send Successfully');
        return redirect()->back();
  }

 public function markNotification(Request $request)
    {
      
        auth()->user()
            ->unreadNotifications
            ->when($request->input('id'), function ($query) use ($request) {
                return $query->where('id', $request->input('id'));
            })
            ->markAsRead();

        return response()->noContent();
    }


   public function displayNotification(){
  	    $user = User::find(1);
         $data['notifications'] = $user->unreadNotifications;
        return view('notification',$data);
 
  }



}  