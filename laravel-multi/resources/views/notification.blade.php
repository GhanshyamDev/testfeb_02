@extends('layouts.customer')

@section('content')
<div class="content-wrapper">
   <div class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
            <h1 class="m-0">   Dashboard</h1>
          </div><!-- /.col -->

          <div class="col-sm-6">
            <ol class="breadcrumb float-sm-right">
              <li class="breadcrumb-item"><a href="#">Home</a></li>
              <li class="breadcrumb-item active">Dashboard v1</li>
            </ol>
          </div> 
        </div> 
      </div> 
    </div>


<section class="content">

 <div class="container-fluid">
     
    <div class="row">
        <div class="col-md-12">
            <div class="card">
                <div class="card-header">Dashboard</div>

                <div class="card-body">
                    <table class="table table-striped projects">
              <thead>
                  <tr >
                      <th style="width: 1%">
                          #
                      </th>
                      <th style="width: 50%">
                          Message
                      </th>
                      <th  style="width: 10%">Date</th>
                      <th style="width: 8%" class="text-center">
                          Status
                      </th>
                   </tr>
              </thead>
              <tbody>
                  
                <?php foreach ($notifications as $notification) { ?>
                <tr  style="height:80px;background-color: #d9edf7;border-color: #bce8f1;">
                <td>  </td>
                <td> {{ $notification->data['message'] }}</td>
                <td> {{ date('d M Y', strtotime($notification->created_at))  }}</td>
                <td>  <a href="#" class="float-right mark-as-read" data-id="{{ $notification->id }}">  Mark as read   </a> </td>
                </tr>
                 <?php } ?>
                
              </tbody>
          </table>
                          
                </div>
            </div>
        </div>
    </div>
  </div>
 </div>
</div>

@endsection
