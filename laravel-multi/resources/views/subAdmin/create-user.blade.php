@extends('layouts.app')

@section('content')
 <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <div class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
            <h1 class="m-0">Dashboard</h1>
          </div><!-- /.col -->
          <div class="col-sm-6">
            <ol class="breadcrumb float-sm-right">
              <li class="breadcrumb-item"><a href="#">Home</a></li>
              <li class="breadcrumb-item active">Dashboard v1</li>
            </ol>
          </div><!-- /.col -->
        </div><!-- /.row -->
      </div><!-- /.container-fluid -->
    </div>
    <!-- /.content-header -->
          
    <!-- Main content -->
    <section class="content">
      <div class="container-fluid">
        <!-- Small boxes (Stat box) -->
        <div class="row">
           <div class="col-md-12">
             <div class="card  ">
                    @if(Session::has('success'))
                <div class="alert alert-success">
                  <strong>Success!</strong> {{Session::get('success')}}
                </div>
                @elseif(Session::has('failed'))
                <div class="alert alert-success">
                  <strong>Failed!</strong> {{Session::get('failed')}}
                </div>
                @endif
                <div class="card-body">
                     
                 <form method="post" action=" {{ url('oemAdmin/userCreate') }}">
                     {{ csrf_field() }}
                     <div class="form-row">
                       <div class="col-md-6">
                            <div class="form-group">
                                <label class="small mb-1" for="inputLastName">Username</label>
                                <input class="form-control" name="name" type="text" placeholder="Enter Name">
                                @if ($errors->has('name'))
                                  <span class="text-danger">{{ $errors->first('name') }}</span>
                                  @endif
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="form-group">
                                <label class="small mb-1" for="inputFirstName">Email</label>
                                 <input class="form-control" name="email" type="email" placeholder="Enter Email">
                                  @if ($errors->has('email'))
                                  <span class="text-danger">{{ $errors->first('email') }}</span>
                                  @endif
                            </div>
                        </div>
                        
                    </div>
                     <div class="form-row">
                        <div class="col-md-6">
                            <div class="form-group">
                                <label class="small mb-1" for="inputFirstName">Phone</label>
                                 <input class="form-control @error('phone') is-invalid @enderror" maxlength="10" name="phone" type="text" placeholder="Enter Phone No">
                                  @if ($errors->has('phone'))
                                  <span class="text-danger">{{ $errors->first('phone') }}</span>
                                  @endif
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="form-group">
                                <label class="small mb-1" for="inputFirstName">Password</label>
                                 <input class="form-control" name="password" type="password" placeholder="Enter Password">
                                  @if ($errors->has('password'))
                                  <span class="text-danger">{{ $errors->first('password') }}</span>
                                  @endif
                            </div> 
                        </div>
                      
                    </div>
                     <div class="form-row">
                        <div class="col-md-6">
                            <div class="form-group">
                                <label class="small mb-1" for="inputFirstName">Category</label>
                                 <select class="form-control  @error('category') is-invalid @enderror  " name="category" onchange="getStateCity()" style="width: 100%;"  autofocus>
                                   <option value=" ">Select Category</option>
                                     @if (count($states))
                                           @foreach ($states as $state)  
                                            <option value="{{ $state['state_id']}}">{{ $state['state_name']}}</option>
                                           @endforeach
                                      @else
                                    <option>State Not Found</option> 
                                    @endif
                                    </select>
                                  @if ($errors->has('category'))
                                  <span class="text-danger">{{ $errors->first('category') }}</span>
                                  @endif
                            </div>
                        </div>
                         <div class="col-md-6">
                            <div class="form-group">
                                <label class="small mb-1" for="inputFirstName">Geography Location</label>
                                 <input class="form-control" name="geography" type="text" placeholder="Enter Your Geography Location">
                                  @if ($errors->has('geography'))
                                  <span class="text-danger">{{ $errors->first('geography') }}</span>
                                  @endif
                            </div>
                        </div>
                      </div>  
                   <!--  <div class="form-row">
                        <div class="col-md-6">
                            <div class="form-group">
                            <label class="small mb-1" for="inputFirstName">Select Role</label>
                               <select class="form-control" name="roleID">
                               <option value=" ">Select New User Role</option>
                               @if (count($roles))
                                 @foreach ($roles as $role)  
                                  <option value="{{ $role['id']}}">{{ $role['name']}}</option>
                                 @endforeach
                                @else
                                <option>UserRole Not Found</option> 
                                @endif
                                </select>
                                 @if ($errors->has('roleID'))
                                  <span class="text-danger">{{ $errors->first('roleID') }}</span>
                                  @endif
                             </div>
                        </div>
                        <div class="col-md-6">
                            <div class="form-group">
                            <label class="small mb-1" for="inputFirstName">Select Permission</label>
                                <select class=" select2" multiple="multiple" name="permission[]" style="width: 100%;color: black;">
                               <option value="NUll" selected>Assign Permission</option>
                               @if (count($permissions))
                                 @foreach ($permissions as $per)  
                                  <option value="{{ $per['id']}}">{{ $per['name']}}</option>
                                 @endforeach
                                @else
                                <option>UserRole Not Found</option> 
                                @endif
                                </select>
                                 @if ($errors->has('permission'))
                                  <span class="text-danger">{{ $errors->first('permission') }}</span>
                                  @endif
                             </div>
                        </div>
                    </div> -->
                     
                    <div class="form-row">
                        <div class="col-md-2">
                            <input type="submit" class="btn btn-primary btn-block" name="submit" value="Create"> 
                        </div>
                    </div>
                    <div class="form-group mt-4 mb-0"></div>
                </form>
                </div>
             </div>


               <div class="card ">
                  <div class="card-header">
                      <i class="fas fa-table mr-1"></i>
                      DataTable Example
                  </div> 
                  <div class="card-body">
                      <div class="table-responsive">
                          <table class="table table-bordered dataTables " id=" " width="100%" cellspacing="0">
                          <thead>
                              <tr>
                                  <th>Sr NO</th>
                                  <th>Name</th>
                                  <th>Assign Role</th>
                                  <th>Approve</th>
                                  <th>Active</th>
                              </tr>
                          </thead>
                          <tbody>
                          @if (count($oemUsers))
                             @foreach ($oemUsers as $user)  
                               <tr>
                                 <td>{{ $user['id'] }}</td>
                                 <td>{{ $user['name'] }}</td>
                                 <td>{{ $user['email'] }}</td>
                                   <td>
                                    @if ($user['approve'] == 'Pending') 
                                    
                                  <!--     <input type="checkbox" class="ApproveUser" data="{{ $user['id'] }}" data-id="app_{{ $user['id'] }}" data-toggle="toggle" value="Success" data-size="md"> -->
                                  <input type="checkbox" checked data-toggle="toggle" data-size="xs">
                                    @endif 
                                    @if ($user['approve'] == 'Success') 
                                    <input type="checkbox" checked data-toggle="toggle" data-size="xs"> 
                                      <!-- <input type="checkbox" data-id="app_{{ $user['id'] }}" data="{{ $user['id'] }}" class="ApproveUser" checked data-toggle="toggle" value="Pending" data-size="sm"> -->

                                     @endif 
                                     <span id="approveMsg_{{ $user['id'] }}"></span>
                                   </td>
                                  <td> <a href="{{ url('oemAdmin/userEdit') }}/{{ Crypt::encryptString($user['id']) }}" class="btn btn-primary btn-sm"><i class="fas fa-edit"></i></a>   
                                    <a href="{{ url('oemAdmin/deleteUser') }}/{{ Crypt::encryptString($user['id']) }}" class="btn btn-danger btn-sm"><i class="fa fa-trash" aria-hidden="true"></i></a>            
                                         </td>
                               </tr>
                             @endforeach
                            @else
                            <tr>UserRole Not Found</tr> 
                            @endif
                         </tbody>
                          </table>

                      </div>
                  </div>
              </div>
        
          </div>
        </div>
         <!-- /.row (main row) -->
      </div><!-- /.container-fluid -->
    </section>
    <!-- /.content -->
  </div>
@endsection
