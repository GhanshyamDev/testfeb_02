@extends('layouts.app')

@section('content')
 <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <div class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
               <h1 class="mb-3">Category</h1>
          </div><!-- /.col -->
          <div class="col-sm-6"> 
            <ol class="breadcrumb float-sm-right">
              <li class="breadcrumb-item"><a href="#">Home</a></li>
              <li class="breadcrumb-item active">Dashboard v1</li>
            </ol>
          </div><!-- /.col -->
        </div><!-- /.row -->
      </div><!-- /.container-fluid -->
    </div>
    <!-- /.content-header -->

    <!-- Main content -->
    <section class="content">
      <div class="container-fluid">
       @if(Session::has('success'))
            <div class="alert alert-success alert-dismissible">
                <button type="button" class="close" data-dismiss="alert">×</button>
                {{Session::get('success')}}
            </div>
        @elseif(Session::has('failed'))
            <div class="alert alert-danger alert-dismissible">
                <button type="button" class="close" data-dismiss="alert">×</button>
                {{Session::get('failed')}}
            </div>
        @endif
        <div class="row">
             <div class="col-md-4 stretch-card grid-margin">
                <div class="card ">
                  <div class="  card-body" style="padding: 2.5rem 1.5rem;">
                   <a class="text-primary f2 active"  href="{{ route('sendServiceReqAll') }}" onclick="event.preventDefault(); document.getElementById('Send-Service-notification-form').submit();"><i class="mdi mdi-cellphone-android"></i><span>Send Service Notification</span></a>
                     
                      <form id="Send-Service-notification-form" action="{{ route('sendServiceReqAll') }}" method="POST" style="display: none;">
                        @csrf
                     </form> 
                  </div>
                </div>
              </div>
              <div class="col-md-4 stretch-card grid-margin">
                <div class="card ">
                  <div class="text-center pl-1 pr-1 card-body" >
                    <a class="text-primary f2" href="#"><i class="mdi mdi-autorenew"></i> <span>Renew AMC</span></a>
                  </div>
                  
                </div>
              </div> 
              <div class="col-md-4 stretch-card grid-margin">
                <div class="card ">
                  <div class="text-center pl-1 pr-1 card-body">
                    <a class="text-primary f2" href="#"><i class="mdi mdi-eye"></i>  <span>View Service Request</span></a>
                  </div>
                  
                </div>
              </div> 
         </div>
         </div><!-- /.container-fluid -->
    </section>
    <!-- /.content -->
  </div>
@endsection
