@extends('layouts.customer')

@section('content')
 <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <div class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
            <h1 class="m-0">Renew Request</h1>
          </div><!-- /.col -->

          <div class="col-sm-6">
            <ol class="breadcrumb float-sm-right">
              <li class="breadcrumb-item"><a href="#">Home</a></li>
              <li class="breadcrumb-item active">Renew Request</li>
            </ol>
          </div><!-- /.col -->
        </div><!-- /.row -->
      </div><!-- /.container-fluid -->
    </div>
    <!-- /.content-header --> 
   <!-- Main content -->
    <section class="content">
      <div class="container-fluid">
        <!-- Small boxes (Stat box) -->
          
        <!-- /.row -->
        <div class="row">
          <div class="col-12">
            <div class="card">
        <div class="card-header">
          <div class="card-tools">
            <button type="button" class="btn btn-tool" data-card-widget="collapse" title="Collapse">
              <i class="fas fa-minus"></i>
            </button>
            <button type="button" class="btn btn-tool" data-card-widget="remove" title="Remove">
              <i class="fas fa-times"></i>
            </button>
          </div>
        </div>
        <div class="card-body">
          <div class="row">
            <div class="col-12 col-md-12 col-lg-8 order-2 order-md-1">
              
              <div class="row">
                <div class="col-12">
                  
                  <div class="post">
                     
                  <form method="POST" action="{{ route('users.postTicket') }}">
                    {{ csrf_field() }}
                    <div class="card-body">
                      <div class="form-group">
                        <label for="exampleInputEmail1">Renew Service</label>
                        <select class="form-control"  name="service_type"  >
                        <option value=" ">Select Service</option>
                           <option value="AMC">AMC</option>
                          <option value="EXTENDED_WARRANTY">Extended Warranty</option>
                       </select>
                        @if ($errors->has('service_type'))
                            <span class="text-danger">{{ $errors->first('service_type') }}</span>
                            @endif
                      </div>
                      <div class="form-group">
                        <label for="exampleInputPassword1">Message(<span style="color: #db5252;font-size:15px;">Max. 100 char required</span>)</label>
                        <textarea  name="description" maxlength="100" class="form-control textareaCount"  ></textarea>
                         @if ($errors->has('description'))
                            <span class="text-danger">{{ $errors->first('description') }}</span>
                            @endif
                      </div>
                      
                    </div>
                    <!-- /.card-body -->
                    <div class="card-footer">
                     <input type="submit" class="btn btn-primary btn-block" name="submit" value="Send">
                    </div>
                  </form>
                   <script type="text/javascript">
                        $('textarea').maxlength({
                                 alwaysShow: true,
                                threshold: 10,
                                warningClass: "label label-success",
                                limitReachedClass: "label label-danger",
                                separator: ' out of ',
                                preText: 'You write ',
                                postText: ' chars.',
                                validate: true
                            });
                      </script>
                  </div>
                </div>
              </div>
            </div>
            <div class="col-12 col-md-12 col-lg-4 order-1 order-md-2">
              <h3 class="text-primary"><i class="fas fa-paint-brush"></i>Product Info</h3>
               
              <br>
              <div class="text-muted">
                 <p class="text-sm">Product Name
                  <b class="d-block">{{ $userProductDetails['product_name'] }}</b>
                </p>
                <p class="text-sm">Manufacturer
                  <b class="d-block">{{ $userProductDetails['category'] }}</b>
                </p>
                <p class="text-sm">Product Warranty 
                  <b class="d-block">{{ $userProductDetails['product_warranty'] }}</b>
                  <?php 
                  $to = \Carbon\Carbon::createFromFormat('Y-m-d H:s:i', $userProductDetails['warranty_start']);
                  $from = \Carbon\Carbon::createFromFormat('Y-m-d H:s:i', $userProductDetails['warranty_end']);
                  $diff_in_days = $to->diffInDays($from);  
                    if($diff_in_days == 1)   { ?> 
                     
                      <p>{{ $diff_in_days  }} Days <span class="badge badge-warning">Expire Soon</span></p>
                      <?php  }  elseif($diff_in_days == 0) { ?>
                     
                         <p>{{ $diff_in_days  }} Days <span class="badge badge-danger">Expire </span></p>
                   
                    <?php  }  else{ ?>
                    
                       <p>{{ $diff_in_days  }} Days <span class="badge badge-success">In_warranty</span></p>
                     
                      <?php } ?>
                </p>
              </div>

              <h5 class="mt-5 text-muted">Project files</h5>
              <ul class="list-unstyled">
                <li>
                  <a href="" class="btn-link text-secondary"><b>Purchase Date:</b> {{ date('d M Y', strtotime($userProductDetails['pauchase_date']))  }}</a>
                </li>
                <li>
                  <a href="" class="btn-link text-secondary"><b>Warranty Date:</b> {{ date('d M Y', strtotime($userProductDetails['warranty_start']))  }}</a>
                </li>
                <li>
                  <a href="" class="btn-link text-secondary"><b>Warranty End:</b>{{ date('d M Y', strtotime($userProductDetails['warranty_end']))  }}</a>
                </li>
               <!--  <li>
                  <a href="" class="btn-link text-secondary"><i class="far fa-fw fa-image "></i> Logo.png</a>
                </li>
                <li>
                  <a href="" class="btn-link text-secondary"><i class="far fa-fw fa-file-word"></i> Contract-10_12_2014.docx</a>
                </li> -->
              </ul>
              <div class="text-center mt-5 mb-3">
                
              </div>
            </div>
          </div>
        </div>
        <!-- /.card-body -->
      </div>
             
          </div>
          <!-- /.col -->
        </div>
        <!-- Main row -->
        
        <!-- /.row (main row) -->
      </div><!-- /.container-fluid -->
    </section>
    <!-- /.content -->
  </div>
@endsection
