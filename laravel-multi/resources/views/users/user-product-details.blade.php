@extends('layouts.customer')

@section('content')
<div class="content-wrapper">
  <!-- Content Header (Page header) -->
  <div class="content-header">
    <div class="container-fluid">
      <div class="row mb-2">
        <div class="col-sm-6">
          <h1 class="m-0" id="headingPrint">   <h1>Products Detail</h1></h1>
        </div><!-- /.col -->

        <div class="col-sm-6">
          <ol class="breadcrumb float-sm-right">
            <li class="breadcrumb-item"> <a  class="btn btn-primary btn-sm" href="#"><i class="fas fa-download" style="font-size: 15px;"></i> Terms & Condition</a> 
            </li> 
            <li class="breadcrumb-item"> <a  class="btn btn-primary btn-sm" href="#"><i class="fas fa-download" style="font-size: 15px;"></i>Invoice</a> 
            </ol>
          </div><!-- /.col -->
        </div><!-- /.row -->
      </div><!-- /.container-fluid -->
    </div>
    <!-- /.content-header --> 
    <!-- Main content -->
    <section class="content">
      <div class="container-fluid"  id="pageDIv">

         <div class="row">
          <div class="col-12">
            <div class="card">

              <div class="card-body"  id="reportPrinting">
                 <div class="row invoice-info">
                  <div class="col-sm-4 invoice-col">

                    <h4>User  Details</h4>
                    <table>
                      <tbody>
                        <tr><td><b>Name</b></td><td>{{ session('Customer_logged')['name'] }}</td></tr>
                        <tr><td><b>Email</b></td><td> {{ session('Customer_logged')['email'] }}</td></tr>
                        <tr><td><b>Aadhar</b></td><td>sdfsfs</td></tr>
                        <tr><td><b>Mobile</b></td><td> {{ $userProductDetails['mobile'] }}</td></tr>
                        <tr><td><b>Location</b></td><td> {{ $city['city_name'] }},{{ $state['state_name'] }}, {{ $userProductDetails['pincode'] }}</td></tr>
                      </tbody>
                    </table>

                  </div> 

                  <!-- /.col -->
                  <div class="col-sm-4 invoice-col">

                    <h4>Product  Details</h4>
                    <table>
                      <tbody>
                        <tr><td><b>Name</b></td><td>{{ $userProductDetails['product_name'] }}</td></tr>
                        <tr><td><b>Price</b></td><td>{{ $userProductDetails['product_price'] }}</td></tr>
                        <tr><td><b>Brand</b></td><td>{{ $userProductDetails['category'] }}</td></tr>
                        <tr><td><b>warranty</b></td><td> {{ $userProductDetails['product_warranty'] }}</td></tr>
                      </tbody>
                    </table>

                  </div>
                  <!-- /.col -->
                  <div class="col-sm-4 invoice-col">
                    <br/>
                    <table>
                      <tbody>
                        <tr><td><b>Purchase End</b></td><td>{{ date('d M Y', strtotime($userProductDetails['pauchase_date']))  }}</td></tr>
                        <tr><td><b>Warranty Date</b></td><td>{{ date('d M Y', strtotime($userProductDetails['warranty_start']))  }}</td></tr>
                        <tr><td><b>Warranty Start</b></td><td>{{ date('d M Y', strtotime($userProductDetails['warranty_end']))  }}</td></tr>
                      </tbody>
                    </table>

                  </div>
                  <!-- /.col -->
                </div>
                <hr/>

                <div class="row no-print">
                  <div class="col-12">
                   
                    <button onclick="printReport()" type="button" class="btn btn-outline-info btn-icon-text"> Print <i class="mdi mdi-printer btn-icon-append"></i>
                    </button>
                   <!--   <button type="button" class="btn btn-primary float-right" style="margin-right: 5px;">
                  <i class="fas fa-download"></i> Generate PDF
                  </button> -->
                  </div>
                </div>
             </div>
            </div>
          </div>
       </div>


       <div class="row mt-3">
         <div class="col-lg-4 col-md-3 grid-margin  ">
          <div class="card">
            <div class="card-body text-center">
              <h4 class="card-title"> <img src="{{ asset('img/service.png')}}" style="width:40px;"></h4>
              <p class="card-description"><a class="text-primary f1  " href="tel:123-456-7890">Call Service Center</a></code></p>
             </div>
          </div>
        </div>
         <div class="col-lg-4 col-md-3 grid-margin  ">
          <div class="card">
            <div class="card-body text-center">
              <h4 class="card-title"> <img src="{{ asset('img/renew.webp')}}" style="width:40px;"></h4>
              <p class="card-description"><a href="{{ url('users/ticketCreate') }}/{{ Str::slug($userProductDetails['product_name'], '-') }}" class="text-primary f1  " href="#">Renew Service</a></code></p>
             </div>
          </div>
        </div>

        <div class="col-lg-4 col-md-3 grid-margin  ">
          <div class="card">
            <div class="card-body text-center">
              <h4 class="card-title"> <img src="{{ asset('img/ser-req.png')}}" style="width:40px;"></h4>
              <p class="card-description"> Create Service Request</p>
             </div>
          </div>
        </div> 

        <div class="col-lg-4 col-md-3 grid-margin  ">
          <div class="card">
            <div class="card-body text-center">
              <h4 class="card-title"> <img src="{{ asset('img/whatsapp.png')}}" style="width:40px;"></h4>
              <p class="card-description">WhatsApp</p>
             </div>
          </div>
        </div> 
      </div>
     </div> 
</section>
 
</div>
@endsection
