 <!doctype html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
 <link rel="icon" type="image/icon" sizes="32x32" href="{{ asset('img/favicon.png') }}">
    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>{{ __('User Login') }}</title>
 
   <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,400i,700&display=fallback">
   <link rel="stylesheet" href="{{ asset('assets/fontawesome-free/css/all.min.css') }}">
    <link rel="stylesheet" href="{{ asset('assets/icheck-bootstrap/icheck-bootstrap.min.css') }}">
      <link rel="stylesheet" href="{{ asset('dist/css/adminlte.min.css') }}">
    <!-- Scripts -->
  <!--   <script src="{{ asset('js/app.js') }}" defer></script> -->

    <!-- Fonts -->
    <link rel="dns-prefetch" href="//fonts.gstatic.com">
    <link href="https://fonts.googleapis.com/css?family=Nunito" rel="stylesheet">

    <!-- Styles -->
   <!--  <link href="{{ asset('css/app.css') }}" rel="stylesheet"> -->
</head>
<body class="hold-transition login-page">
<div class="login-box">
  <!-- /.login-logo -->
  <div class="card card-outline card-primary">
    <div class="card-header text-center">
      <p class="h3"><b>Signup</b> </p>
    </div>
    <div class="card-body">
        <div class="card-header">
               @if(Session::has('success'))
                    <div class="alert alert-success alert-dismissible">
                        <button type="button" class="close" data-dismiss="alert">×</button>
                        {{Session::get('success')}}
                    </div>
                @elseif(Session::has('failed'))
                    <div class="alert alert-danger alert-dismissible">
                        <button type="button" class="close" data-dismiss="alert">×</button>
                        {{Session::get('failed')}}
                    </div>
                @endif
        </div>
          
         
       <form method="POST" action="{{ url('users/signUp') }}">
       @csrf
         <div class="form-group">
           <input type="text" class="form-control @error('name') is-invalid @enderror"   name="name" value="{{ old('name') }}" required autocomplete="name" autofocus placeholder="Enter Name">
           @error('name')
            <span class="invalid-feedback" role="alert">  <strong>{{ $message }}</strong> </span>
            @enderror  
         </div>
         <div class="form-group">
            <input type="email" class="form-control @error('email') is-invalid @enderror"   name="email" value="{{ old('email') }}" required autocomplete="email" autofocus placeholder="Enter Email">
             @error('email')
            <span class="invalid-feedback" role="alert">  <strong>{{ $message }}</strong> </span>
            @enderror
         </div>
          <div class="form-group">
            <input type="text" class="form-control @error('mobile') is-invalid @enderror" maxlength="10"  name="mobile" value="{{ old('mobile') }}" required autocomplete="mobile" autofocus placeholder="Enter Mobile">
             @error('mobile')
            <span class="invalid-feedback" role="alert">  <strong>{{ $message }}</strong> </span>
            @enderror


         </div>
          <div class="form-group">
            <input type="password" class="form-control @error('password') is-invalid @enderror"   name="password" value="{{ old('password') }}" required autocomplete="password" autofocus placeholder="Enter Password">
             @error('password')
            <span class="invalid-feedback" role="alert">  <strong>{{ $message }}</strong> </span>
            @enderror
         </div>
           
         <div class="form-group">
             
            <select class="form-control select2 @error('state') is-invalid @enderror stateClss" name="state" onchange="getStateCity()" style="width: 100%;"  autofocus>
               <option value=" ">Select State</option>
                 @if (count($states))
                       @foreach ($states as $state)  
                        <option value="{{ $state['state_id']}}">{{ $state['state_name']}}</option>
                       @endforeach
                  @else
                <option>State Not Found</option> 
                @endif
                </select>
                 @error('state')
                <span class="invalid-feedback" role="alert">  <strong>{{ $message }}</strong> </span>
                @enderror
          </div>
            <div class="form-group">
                 <select type="text" class="form-control @error('city') is-invalid @enderror cityClass" name="city" value="{{ old('city') }}" required   autofocus placeholder="Enter City">
                  <option value=" ">Select City</option>
                 </select>
                 @error('city')
                <span class="invalid-feedback" role="alert">  <strong>{{ $message }}</strong> </span>
                @enderror
           </div>
             <div class="form-group">
                <input type="number" class="form-control @error('pincode') is-invalid @enderror"   name="pincode" value="{{ old('pincode') }}" required autocomplete="pincode" autofocus placeholder="Enter Pincode" pattern="[1-9]{1}[0-9]{9}">  
                    @error('pincode')
                    <span class="invalid-feedback" role="alert">  <strong>{{ $message }}</strong> </span>
                    @enderror
            </div>
          
         
        <div class="row">
          <div class="col-8">
            <div class="icheck-primary ml-3">
                <input class="form-check-input" type="checkbox" name="remember" id="remember" {{ old('remember') ? 'checked' : '' }}>
               
              <label for="remember ">
                {{ __('Remember Me') }}
              </label>
            </div>
          </div>
          <!-- /.col -->
          <div class="col-4">
             
          </div>

          <!-- /.col -->
        </div>
        <div class="social-auth-links text-center mt-2 mb-3">
         <button type="submit" class="btn btn-primary btn-block" style="width: 80px;">  {{ __('Sign In') }}  </button>
        </div>
      </form>
 
      
      @guest
       @if (Route::has('register'))
         <p class="mb-0"><a href="{{ route('register') }}" class="text-center" >{{ __('Register') }}</a></p>
        @endif
        @if (Route::has('password.request'))
        <a href="{{ route('password.request') }}" >
            {{ __('Forgot Your Password?') }}
        </a>
        @endif
        
       @endguest
    </div>
    <!-- /.card-body -->
  </div>
  <!-- /.card -->
</div>
  
<script src=" {{ asset('assets/jquery/jquery.min.js') }}"></script>
<script src="{{ asset('assets/bootstrap/js/bootstrap.bundle.min.js') }}"></script>
<script src=" {{ asset('dist/js/adminlte.js') }}"></script>
<script type="text/javascript">
   function getStateCity(){ 
       var state= $(".stateClss").val();
      
         $.ajax({
          url: "{{ url('users/getCitys') }}",
          type:"POST",
          data:{
            "_token": "{{ csrf_token() }}",
            state:state, 
          },
          success:function(response){
           $.each(response.citys, function(key, value) {
              $(".cityClass").append($('<option></option>').attr('value', value.city_id).text(value.city_name));
             });
             
          },
         });
     
     
};
 
      </script>
</body>
</html>
