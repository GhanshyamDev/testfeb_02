<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\Admin\AdminController;
use App\Http\Controllers\SubAdmin\OemAdminController;
use App\Http\Controllers\TicketController;
use App\Http\Controllers\Notification;
use App\Http\Controllers\HomeController;
/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

// Route::get('/', function () {
//     return view('welcome');
// });
Route::get('/', function () {
   return redirect(route('login'));
});

Auth::routes();
Route::group(['middleware' => 'role:oem'], function() {

Route::get('oemAdmin/userCreate', [OemAdminController::class, 'CreateUser']);
Route::post('oemAdmin/userCreate', [OemAdminController::class, 'CreateUser'])->name('oemAdmin.userCreate');
Route::get('oemAdmin/userEdit/{id}', [OemAdminController::class, 'EditUser']);
Route::post('oemAdmin/updateUser', [OemAdminController::class, 'updateUser']);
Route::get('oemAdmin/deleteUser/{id}', [OemAdminController::class, 'DeleteUser']);

});
Route::get('oemAdmin/viewTicket', [TicketController::class, 'Oemticket']);
Route::get('/home',  [OemAdminController::class, 'index'])->name('home');
Route::post('approveUser', [OemAdminController::class, 'ApproveUser']);

//send notification 
Route::post('sendServiceReqAll', [Notification::class, 'sendServiceNotification'])->name('sendServiceReqAll');

Route::get('oemAdmin/createCategory', [OemAdminController::class, 'createCategory']);
Route::post('createCategory', [OemAdminController::class, 'createCategory'])->name('oemAdmin.createCategory');
Route::get('oemAdmin/categoryEdit/{id}', [OemAdminController::class, 'categoryEdit']); 
Route::post('categoryUpdate', [OemAdminController::class, 'categoryUpdate'])->name('oemAdmin.categoryUpdate'); 
Route::get('oemAdmin/categoryDelete/{id}', [OemAdminController::class, 'categoryDel']); 

Route::get('oemAdmin/createGeography', [OemAdminController::class, 'createGeography']);
Route::post('createGeography', [OemAdminController::class, 'createGeography'])->name('oemAdmin.createGeography');
Route::get('oemAdmin/geographyEdit/{id}', [OemAdminController::class, 'geographyEdit']); 
Route::post('geographyUpdate', [OemAdminController::class, 'geographyUpdate'])->name('oemAdmin.geographyUpdate'); 
 Route::get('oemAdmin/geographyDelete/{id}', [OemAdminController::class, 'geographyDel']); 
//Renew AMC 

Route::get('oemAdmin/ViewRenewAmc',  [OemAdminController::class, 'viewRenew_amcList']);

Route::get('oemAdmin/ViewServiceRequest',  [OemAdminController::class, 'viewService_request']);






 // admin

Route::get('admin/home', [AdminController::class, 'index'])->name('admin.route')->middleware('admin');
Route::get('admin/ticket', [TicketController::class, 'Adminticket'])->middleware('admin');

 


Route::get('viewTicket',  [TicketController::class, 'viewTicket']);